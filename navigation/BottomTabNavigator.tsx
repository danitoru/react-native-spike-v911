/**
 * Learn more about createBottomTabNavigator:
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */

import * as React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { IconButton } from 'react-native-paper';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import TabTwoScreen from '../screens/TabTwoScreen';
import { BottomTabParamList, TabOneParamList, TabTwoParamList } from '../types';

import VolunteerScreen from '../components/Volunteer/screens/VolunteerScreen';
import { VolunteerPersonalData } from '../components/Volunteer/model/VolunteerPersonalData';
import { VolunteerScreenTheme } from '../components/Volunteer/types';
import { CustomSwitchMask } from '../components/CustomSwitch/types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="TabOne"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
      <BottomTab.Screen
        name="TabOne"
        component={TabOneNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="TabTwo"
        component={TabTwoNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

const theme: VolunteerScreenTheme = {
  backgroundColorHeader: "indigo",
  backgroundColorContainer: "white",
  backgroundColorBottom: "indigo",
  showExactTheme: true,
};

const volunteer: VolunteerPersonalData = {
  id: 12345,
  name: "Daniel",
  lastName: "Torres",
  middleName: "Arafat",
  surName: "Ruiz",
  role: "Voluntario",
  isActive: false,
};

const mask: CustomSwitchMask = {
  activeText: "Activo",
  inactiveText: "Inactivo",
  activeIcon: <IconButton icon="heart" color="red" size={40} />,
  inactiveIcon: <IconButton icon="heart-off" color="black" size={40} />,
  spinnerColor: "indigo",
}

const TabOneNavigator = () => {
  const Volunteer = () => <VolunteerScreen
                            theme={theme}
                            volunteer={volunteer}
                            mask={mask}
                          />
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name="TabOneScreen"
        component={Volunteer}
        options={{
          headerShown: false
        }}
      />
    </TabOneStack.Navigator>
  );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="TabTwoScreen"
        component={TabTwoScreen}
        options={{ headerTitle: 'Tab Two Title' }}
      />
    </TabTwoStack.Navigator>
  );
}
