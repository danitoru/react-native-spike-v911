import * as React from 'react';
import { Appbar } from 'react-native-paper';
import ScreenWrapper from '../../../ScreenWrapper';
import CustomSwitch from '../../../CustomSwitch';
import { VolunteerPersonalData } from '../../model/VolunteerPersonalData';
import { VolunteerScreenTheme } from '../../types';
import { CustomSwitchMask } from '../../../CustomSwitch/types';

import { useStyles } from './style';

type VolunteerScreenProps = React.PropsWithChildren<{
  theme: VolunteerScreenTheme;
  volunteer: VolunteerPersonalData;
  mask: CustomSwitchMask;
}>;

const WELCOME = "Bienvenido,";
const ESPACIO = " ";
const SHARP = "#";

const VolunteerScreen: React.FC<VolunteerScreenProps> = (props) => {
  const { volunteer, mask, theme } = props;

  const [isActive, setActive] = React.useState(volunteer.isActive);
  const [isLoading, setLoading] = React.useState(false);

  const onToggleSwitch = (active: boolean): void => {
    setLoading(true);

    setTimeout(function () {
      setActive(active);
      setLoading(false);
    }.bind(this), 1000);
  }

  const fullName = volunteer.name.concat(ESPACIO)
                                 .concat(volunteer.middleName).concat(ESPACIO)
                                 .concat(volunteer.lastName).concat(ESPACIO)
                                 .concat(volunteer.surName);
  const greeting = WELCOME.concat(ESPACIO).concat(fullName);
  const roleId = volunteer.role.concat(ESPACIO).concat(SHARP).concat(volunteer.id.toString());

  const styles = useStyles(theme);

  return (
    <React.Fragment>
      {/** Section Header */}
      <Appbar.Header
          style={styles.header}
          theme={{ mode: "exact" }}
      >
          <Appbar.Content
            title={greeting}
            subtitle={roleId}
            style={styles.headerContent}
          />
      </Appbar.Header>

      {/** Section Main */}
      <ScreenWrapper
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <CustomSwitch
          mask={mask}
          active={isActive}
          loading={isLoading}
          isFootText={true}
          onChange={() => onToggleSwitch(!isActive)}
        />
      </ScreenWrapper>

      {/** Section Footer */}
      <Appbar
        style={styles.bottom}
        theme={{ mode: theme.showExactTheme ? "exact" : "adaptive" }}
      >
        <Appbar.Action icon="email" onPress={() => {}} ></Appbar.Action>
        <Appbar.Content title="Notificaciones" style={styles.headerContent} />
        <Appbar.Action icon="delete" onPress={() => {}} />
      </Appbar>
    </React.Fragment>
  );
};

export default VolunteerScreen;
