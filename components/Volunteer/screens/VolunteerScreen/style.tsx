import { StyleSheet } from 'react-native';
import { VolunteerScreenTheme } from '../../types';

export const useStyles = ( theme: VolunteerScreenTheme ) => StyleSheet.create({
    header: {
      backgroundColor: theme.backgroundColorHeader,
    },
    headerContent: {
      alignItems: 'center',
    },
    container: {
      marginBottom: 56,
      backgroundColor: theme.backgroundColorContainer,
    },
    contentContainer: {
      alignItems: 'center',
      justifyContent: 'center',
      flexGrow: 1,
      paddingBottom: 10,
    },
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: 8,
      paddingHorizontal: 16,
    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: theme.backgroundColorBottom,
    },
    fab: {
      position: 'absolute',
      right: 16,
      bottom: 28,
    },
    paragraph: {
      fontSize: 30,
      paddingTop: 10,
      marginVertical: 30,
    },
});
