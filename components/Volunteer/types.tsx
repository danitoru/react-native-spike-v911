export type VolunteerScreenTheme = {
  backgroundColorHeader: string;
  backgroundColorContainer: string;
  backgroundColorBottom: string;
  showExactTheme: boolean;
};
