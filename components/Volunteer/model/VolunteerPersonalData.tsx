 export interface VolunteerPersonalData {
  id: number;
  name: string;
  lastName: string;
  middleName: string;
  surName: string;
  role: string;
  isActive: boolean;
};
