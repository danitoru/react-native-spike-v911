import * as React from 'react';

export interface CustomSwitchMask {
  activeText: string;
  inactiveText: string;
  spinnerColor: string;
  activeIcon?: React.ReactNode;
  inactiveIcon?: React.ReactNode;
};

export type CustomSwitchProps = React.PropsWithChildren<{
  mask: CustomSwitchMask;
  active: boolean;
  loading: boolean;
  isFootText?: boolean;
  onChange?: () => void;
}>;
