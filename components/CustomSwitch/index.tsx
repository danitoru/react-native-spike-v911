import * as React from 'react';
import { View } from 'react-native';
import {
        Switch,
        Paragraph,
        ActivityIndicator
} from 'react-native-paper';
import { useStyles } from './style';
import { CustomSwitchMask, CustomSwitchProps } from './types';

type TextSwitchProps = React.PropsWithChildren<{
    mask: CustomSwitchMask;
    active: boolean;
    loading: boolean;
    isFootText?: boolean;
    rowStyle: any;
    paragraphStyle: any;
}>;

  
const TextSwitch = (props: TextSwitchProps) => {
    const {
            mask,
            active,
            loading,
            isFootText,
            rowStyle,
            paragraphStyle
    } = props;

    return (
        <View style={rowStyle}>
            {
                loading
                ? <ActivityIndicator
                    animating={loading}
                    color={mask.spinnerColor}
                    style={{ paddingBottom: isFootText ? 0 : 10, paddingTop: isFootText ? 10 : 0, marginVertical: 15 }}
                    size="large"
                  />
                : <React.Fragment>
                    {active ? mask.activeIcon : mask.inactiveIcon}
                    <Paragraph style={paragraphStyle}>
                        {!active ? mask.inactiveText : mask.activeText}
                    </Paragraph>
                  </React.Fragment>    
            }
        </View>
    );
}

const CustomSwitch: React.FC<CustomSwitchProps> = (props) => {
    const { mask, active, loading, isFootText, onChange } = props;
    const styles = useStyles();
    const TextTop = () => {
        return (
            isFootText
            ? null
            : <TextSwitch
                mask={mask}
                active={active}
                loading={loading}
                isFootText={false}
                rowStyle={styles.row}
                paragraphStyle={styles.paragraph as any}
              />
        );
    }
    const TextBottom = () => {
        return (
            isFootText
            ? <TextSwitch
                mask={mask}
                active={active}
                loading={loading}
                isFootText={true}
                rowStyle={styles.row}
                paragraphStyle={styles.paragraph as any}
                />
            : null
        );
    }
    const ViewSwitch = () => <View style={styles.row}>
                                <Switch
                                    color="green"
                                    style={{ transform: [{ scaleX: 4 }, { scaleY: 4 }] }}
                                    value={active}
                                    onValueChange={onChange}
                                />
                             </View>
    return (
        <React.Fragment>
            <TextTop />
            <ViewSwitch />
            <TextBottom />
        </React.Fragment>
    );
}

export default CustomSwitch;
