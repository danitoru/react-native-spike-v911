import { StyleSheet } from 'react-native';

export const useStyles = () => StyleSheet.create({
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: 8,
      paddingHorizontal: 16,
    },
    paragraph: {
      fontSize: 30,
      paddingTop: 10,
      marginVertical: 30,
    },
});
